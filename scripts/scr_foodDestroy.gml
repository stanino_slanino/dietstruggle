instFood = collision_circle(x, y, 50, obj_food, false, true);
if (instFood != noone)
{
    if (instFood.status != "attached")
    {
        instFood.status = "death";
        
        audio_play_sound(so_cirkular, 10, false);
        instance_create(480, 330, obj_splash);
    }
}
