instance_create(0,0,obj_powerUp);

randomPowerUp = irandom(1);

// Reverzna gravitacia
if (randomPowerUp == 0)
{
    global.foodGravity *= -1;
}

// Rychlejsi pas s jedlom
if (randomPowerUp == 1)
{
    global.foodSpeed *= 2;
    global.foodSpawnTime = 0.5;
}


