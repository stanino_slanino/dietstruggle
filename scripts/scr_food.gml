if (global.foodRotation == 1)
{
    image_angle = direction;
}

if (status == "default")
{

}

if (status == "moving")
{
    if (x <= room_width/2)
    {
        x += global.foodSpeed;
    }
    if (x >= room_width/2)
    {
        x -= global.foodSpeed;
    }
    
    direction = 0;
    speed = 0;
    gravity = 0;
}

if (status == "attached")
{
   friction = 0; 
   if (playerHand == 0)
   {
        x = obj_player0.x;
        y = obj_player0.y;
        
        direction = 0;
        speed = 0;
        gravity = 0;
   }
   
   if (playerHand == 1)
   {
        x = obj_player1.x;
        y = obj_player1.y;
        
        direction = 0;
        speed = 0;
        gravity = 0;
   }
   
}

if (status == "falling")
{ 
    
    if (y >= 440 && global.foodFallMode == "return")
    {
        status = "moving";
        audio_play_sound(so_stick,10,false);
    }

    if (y >= room_height)
    {
        status = "death";
    }
    
    // resolves wall collision behaviour
    
    // "reset"  = moves instance to the opposite side
    if global.wallMode = "reset"
    {
        if x >= room_width + sprite_width*0.5
        {
            x = 0;
            audio_play_sound(so_teleport,10,false);
        }
        if x <= 0 - sprite_width*0.5
        {
            x = room_width;
            audio_play_sound(so_teleport,10,false);
        }
    }
    
    //"bounce" - bounces the instance from the wall
    if global.wallMode = "bounce"
    {
        if x >= room_width
        {   
            x = room_width - 1;
            curDir = direction;
            speed *= global.wallBounciness;
            if curDir == 180 {direction = 0;}
            else if curDir < 180 {direction = 0 + 180 - curDir;}
            else {direction = 360 - curDir - 180}
            audio_play_sound(so_odraz,10,false);
        }
        
        else if x <= 0
        {
            curDir = direction;
            x = 1;
            speed *= global.wallBounciness;
            if curDir == 0 {direction = 180;}
            else if curDir > 270 {direction = curDir - 270 - curDir;}
            else {direction = 180 - curDir;}
            audio_play_sound(so_odraz,10,false);
        }
    }
    
    //"sticky" - food sticks to the wall

    if global.wallMode = "sticky"
    {
        if x <= 0
        {   
            x = 5;
            speed = 0;
            gravity = 0.31;
            friction = 0.28;
            audio_play_sound(so_stick,10,false);
        } 
        else if x >= room_width
        {
            x = room_width -5;
            speed = 0;
            gravity = 0.31;
            friction = 0.28;
            audio_play_sound(so_stick,10,false);
        }
    }
    
}

if (status == "death")
{
    instance_destroy();
}
