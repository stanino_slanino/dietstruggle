///resolve food-wall collision

if (status == "falling")
{     
    // resolves wall collision behaviour
    
    // "reset"  = moves instance to the opposite side
    if global.wallMode = "reset"
    {
        if x >= room_width + sprite_width*0.5
        {
            x = 0;
        }
        if x <= 0 - sprite_width*0.5
        {
            x = room_width;
        }
    }
    
    //"bounce" - bounces the instance from the wall
    if global.wallMode = "bounce"
    {
        if x >= room_width
        {   
            x = room_width - 1;
            show_debug_message(direction);
            show_debug_message("---")
            curDir = direction;
            speed *= global.wallBounciness;
            if curDir == 180 {direction = 0;}
            else if curDir < 180 {direction = 0 + 180 - curDir;}
            else {direction = 360 - curDir - 180}
            show_debug_message(curDir);
            show_debug_message(direction);
            show_debug_message("---")
        }
        
        else if x <= 0
        {
            curDir = direction;
            x = 1;
            speed *= global.wallBounciness;
            if curDir == 0 {direction = 180;}
            else if curDir > 270 {direction = curDir - 270 - curDir;}
            else {direction = 180 - curDir;} 
        }
    }
}
